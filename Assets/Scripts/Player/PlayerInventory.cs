using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerInventory : MonoBehaviour
{
    public List<Item> Items = new List<Item>();
    public List<GameObject> itemsObjects = new List<GameObject>();

    [SerializeField] public Transform inventoryItemsPlaces;
    [SerializeField] public int inventorySpace;
    [SerializeField] public GameObject cloneObject;
    [SerializeField] public Item pickedUpItem;
    [SerializeField] public int currentItemsCount;

    [SerializeField] public int Money;
    [SerializeField] private Text MoneyText;
    
    

    private void Update()
    {
        MoneyText.text = "Денег" + " " + Money.ToString();
        
    }
}
