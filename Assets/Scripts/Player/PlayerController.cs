using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody), typeof(BoxCollider))]
public class PlayerController : MonoBehaviour
{
    [SerializeField] public Animator playerAnimator;
    [SerializeField] private Rigidbody rigidbody;
    [SerializeField] private FixedJoystick fixedJoystick;

    [SerializeField] private float speed;
    [SerializeField] private bool isRunning;
    [SerializeField] public PlayerInventory PlayerInventory;
    public bool ItemsTaken;

    private void Start()
    {
        PlayerInventory = FindObjectOfType<PlayerInventory>();
    }

    private void FixedUpdate()
    {
        rigidbody.velocity = new Vector3(fixedJoystick.Horizontal * speed, rigidbody.velocity.y, fixedJoystick.Vertical * speed);

        if (fixedJoystick.Horizontal != 0 || fixedJoystick.Vertical != 0)
        {
            transform.rotation = Quaternion.LookRotation(rigidbody.velocity);
            playerAnimator.SetBool("IsWalking",true);
        }
        else
        {
            playerAnimator.SetBool("IsWalking",false);
        }
        
    }

    private void Update()
    {
        if (PlayerInventory.itemsObjects.Count != 0)
        {
            ItemsTaken = true;
        }
        else if(PlayerInventory.itemsObjects.Count == 0)
        {
            ItemsTaken = false;
        }
        
        playerAnimator.SetBool("ItemsTaken",ItemsTaken);
    }
}
