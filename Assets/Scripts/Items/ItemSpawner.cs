using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemSpawner : MonoBehaviour
{
    public List<Transform> itemsPlaces = new List<Transform>();
    public List<Item> storageInventory = new List<Item>();
    public List<GameObject> itemsInPlaces = new List<GameObject>();

    public int currentStorageUsedPlaces;
    
    [SerializeField] private Item currentStorageItem;
    [SerializeField] private int storageInventorySpace;

    [SerializeField] private float timeToSpawn;
    [SerializeField] private float timeToTake;

    [SerializeField] private GameObject cloneObject;

    [SerializeField] private GameObject itemPrefab;

    [SerializeField] private PlayerInventory playerInventory;


    private void Start()
    {
        playerInventory = FindObjectOfType<PlayerInventory>();
        StartCoroutine(SpawnObjectsInPlaces());
        storageInventorySpace = itemsPlaces.Count;
    }

    private void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        StopAllCoroutines();
        StartCoroutine(AddItemsToPlayer());
    }

    private void OnTriggerExit(Collider other)
    {
        StopAllCoroutines();
        StartCoroutine(SpawnObjectsInPlaces());
    }

    IEnumerator SpawnObjectsInPlaces()
    {
        yield return new WaitForSeconds(timeToSpawn);
        cloneObject = Instantiate(itemPrefab, itemsPlaces[itemsInPlaces.Count].position, itemsPlaces[itemsInPlaces.Count].rotation);
        itemsInPlaces.Add(cloneObject);
        storageInventory.Add(currentStorageItem);
        currentStorageUsedPlaces++;

        if (itemsInPlaces.Count < storageInventorySpace)
        { 
            StartCoroutine(SpawnObjectsInPlaces());
        }
        else if(itemsInPlaces.Count == storageInventorySpace)
        { 
            StopAllCoroutines();
        }
    }

    IEnumerator AddItemsToPlayer()
    {
        yield return new WaitForSeconds(timeToTake);
        
        playerInventory.pickedUpItem = currentStorageItem;
        playerInventory.cloneObject = Instantiate(itemPrefab,playerInventory.inventoryItemsPlaces);
        playerInventory.cloneObject.transform.position = playerInventory.inventoryItemsPlaces.position;
        
        playerInventory.Items.Add(currentStorageItem);
        playerInventory.itemsObjects.Add(playerInventory.cloneObject);
        playerInventory.currentItemsCount++;
        
        Destroy(itemsInPlaces[currentStorageUsedPlaces - 1]);
        storageInventory.Remove(storageInventory[currentStorageUsedPlaces-1]);
        itemsInPlaces.Remove(itemsInPlaces[currentStorageUsedPlaces-1]);
        currentStorageUsedPlaces--;

        if (playerInventory.currentItemsCount >= playerInventory.inventorySpace)
        {
            StopAllCoroutines();
        }
        else
        {
            StartCoroutine(AddItemsToPlayer());
        }

    }

    
}
