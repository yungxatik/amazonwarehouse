using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaleItems : MonoBehaviour
{
    public List<Transform> itemPlaces = new List<Transform>();
    public List<GameObject> itemsInPalettas = new List<GameObject>();
    [SerializeField] private int currentPalletasItemsCount; 
    [SerializeField] private Item currentPalettasItem;
    [SerializeField] private int palettasSpace;
    [SerializeField] private float timeToPlace;
    [SerializeField] private float timeToSell;
    [SerializeField] private PlayerInventory playerInventory;
    [SerializeField] private GameObject cloneObject;
    [SerializeField] private GameObject saleEffect;

    private void Start()
    {
        playerInventory = FindObjectOfType<PlayerInventory>();
    }

    private void OnTriggerEnter(Collider other)
    {
        StopAllCoroutines();
        StartCoroutine(PlaceItems());
        currentPalettasItem = playerInventory.pickedUpItem;
        cloneObject = playerInventory.cloneObject;
    }

    private void OnTriggerExit(Collider other)
    {
        StopAllCoroutines();
        StartCoroutine(SellItems());
    }

    IEnumerator PlaceItems()
    {
        yield return new WaitForSeconds(timeToPlace);
        currentPalletasItemsCount++;

        cloneObject = Instantiate(playerInventory.cloneObject, itemPlaces[itemsInPalettas.Count].position,
            itemPlaces[itemsInPalettas.Count].rotation);
        itemsInPalettas.Add(cloneObject);
        
        Destroy(playerInventory.itemsObjects[currentPalletasItemsCount - 1]);
        playerInventory.Items.Remove(playerInventory.Items[playerInventory.currentItemsCount - 1]);
        
        playerInventory.currentItemsCount--;

        if (playerInventory.currentItemsCount != 0 && itemsInPalettas.Count < palettasSpace)
        {
            StartCoroutine(PlaceItems());
        }
        else if(currentPalletasItemsCount == palettasSpace)
        {
            StopAllCoroutines();
            for (int i = 0; i < playerInventory.itemsObjects.Count; i++)
            {
                playerInventory.itemsObjects.Clear();
            }
        }
        else if(playerInventory.Items.Count == 0)
        {
            for (int i = 0; i < playerInventory.itemsObjects.Count; i++)
            {
                playerInventory.itemsObjects.Clear();
            }
        }

    }

    IEnumerator SellItems()
    {
        yield return new WaitForSeconds(timeToSell);
        playerInventory.Money += currentPalettasItem.Cost;
        Instantiate(saleEffect);
        Destroy(itemsInPalettas[currentPalletasItemsCount - 1]);
        itemsInPalettas.Remove(itemsInPalettas[currentPalletasItemsCount - 1]);
        currentPalletasItemsCount--;

        if (itemsInPalettas.Count != 0)
        {
            StartCoroutine(SellItems());
        }
        else
        {
            StopAllCoroutines();
        }
    }
}
