
using System;
using System.Collections;
using UnityEngine;

public class BuyPlace : MonoBehaviour
{
    [SerializeField] private GameObject place;
    [SerializeField] private float placeCost;
    [SerializeField] private PlayerInventory PlayerInventory;
    [SerializeField] private float fillTime;

    private void Start()
    {
        PlayerInventory = FindObjectOfType<PlayerInventory>();
    }

    private void OnTriggerEnter(Collider other)
    {
        
    }

    IEnumerator StartFillMoney()
    {
        yield return new WaitForSeconds(fillTime);
        
    }
}
