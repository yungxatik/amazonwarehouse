using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Item", menuName = "Items")]
public class Item : ScriptableObject
{
    [SerializeField] private string _name;
    [SerializeField] private GameObject _objectLook;
    [SerializeField] private int _cost;

    public string Name => _name;
    public GameObject ObjectLook => _objectLook;
    public int Cost => _cost;
}
